# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _


class DynamiquePageBackend(models.Model):
    _name = "dynamique.page.backend"
    _description = "Dynamique Page backend"
    _order = "id asc"
    
    
    name = fields.Char('Name')
    slider = fields.Many2one(comodel_name="slider.backend", string="Slider", required=False, )
    page = fields.Many2one(comodel_name="website.page", string="Page", required=False, )
    bloc_field_ids = fields.One2many(comodel_name="page.bloc", inverse_name="bloc_field_id", string="", required=False, )

class PageBloc(models.Model):
    _name = "page.bloc"
    _description = "Dynamique Page bloc"
    _order = 'sequence, id'

    bloc_field_id = fields.Many2one(comodel_name="dynamique.page.backend", string="", required=False, )
    name = fields.Char('Name',related="bloc_id.name")
    sequence = fields.Integer(help="Determine the display order", default=1)
    bloc_id = fields.Many2one(comodel_name="website.dynamique.bloc", string="", required=False, )
    section = fields.Selection(string="Section", selection=[('section1', 'section 1'), ('section2', 'section2'), ('section3', 'section3'),], required=False, )
