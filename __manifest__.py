{
    'name': 'website dynamique backend',
    'summary': 'sekretaire',
    'description': 'dev info',
    'category': '',
    'version': '1.0',
    'author': 'Intelligent Concepts/Fatma Ezzahra Ben Hmida',
    'licence': 'GRL',
    'Website': '',
    'depends': ['website_slider','website','website_dynamique_bloc'],
    'data': [
        'security/ir.model.access.csv',
        'views/website_dynamique_backend.xml',
        'views/dynamique_page_backend_view.xml',

    ],

}
